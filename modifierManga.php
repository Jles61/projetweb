<?php

require("fonctions.php");
include("Attribut.php");
initheader();
if ($_POST) {
    $liste= array($_POST['Id'],$_POST['titre'],$_POST['nomAuteur'],$_POST['prenomAuteur'],$_POST['nomDessinateur'],$_POST['prenomDessinateur'],$_POST['genre'],$_POST['annee']);
    update($liste);
    alert("success","Le Manga à bien été mis à jour");
}else{
    $listeAttr = array(
    new Attribut ("Id", "number", "Id"),
    new Attribut ("titre", "text", "Titre"),
    new Attribut ("nomAuteur", "text", "Nom de l'auteur"),
    new Attribut ("prenomAuteur", "text", "Prénom de l'auteur"),
    new Attribut ("nomDessinateur", "text", "Nom du dessinateur"),
    new Attribut ("prenomDessinateur", "text", "Prénom du dessinateur"),
    new Attribut ("genre", "text", "Genre"),
    new Attribut ("annee", "number", "Année")
);
    form("Modifier la base","POST","modifierManga.php","Modifier",$listeAttr,getMangaFromId($_GET['Id']));
}

initfooter();

?>