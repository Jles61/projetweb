<?php
date_default_timezone_set('Europe/Paris');
try{
	require("connexion.php");
	require("fonctions.php");
	initheader();
	$file_db=connect_bd();
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	if ($_GET['Id']){
		$sth=$file_db->query('SELECT Titre,NomAuteur,PrenomAuteur,NomDessinateur,PrenomDessinateur,Genre,Annee FROM MANGA WHERE IDmanga=='.(int)$_GET['Id'].'');
	}
	$args = array('Titre','Nom de l\'auteur','Prénom de l\'auteur','Nom du dessinateur','Prénom du dessinateur','Genre','Année de publication');

	toCard($sth,$args);
	bouton("modifierManga.php?Id=".$_GET['Id'],"Modifier");
  // on ferme la connexion
  $file_db=null;
  initfooter();
}
catch(PDOException $ex){
  echo $ex->getMessage();
}
?>