<?php

require("fonctions.php");
include("Requete.php");
include("Attribut.php");
initheader();
$listeAttr = array(
    new Attribut ("titre", "text", "Titre"),
    new Attribut ("nomAuteur", "text", "Nom de l'auteur"),
    new Attribut ("prenomAuteur", "text", "Prénom de l'auteur"),
    new Attribut ("nomDessinateur", "text", "Nom du dessinateur"),
    new Attribut ("prenomDessinateur", "text", "Prénom du dessinateur"),
    new Attribut ("genre", "text", "Genre"),
    new Attribut ("annee", "number", "Année")
);

if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    form("Ajout dans la BD","POST","addBD.php","Ajouter",$listeAttr=$listeAttr);
}
else {
    $listeA = array();
    foreach ($listeAttr as $a) {
        if ($_POST[$a->get_name()] == NULL) array_push($listeA, "");
        else array_push($listeA, $_POST[$a->get_name()]);
    }
    if ($listeA[0] == "" || $listeA[5] == "" || $listeA[6] == "") alert("warning","Le Manga n'as pas pu être ajouté à cause du manque d'information");
    else {
        $req = new Requete($listeA[0], $listeA[1], $listeA[2], $listeA[3], $listeA[4], $listeA[5], $listeA[6]);
        $req->insertion();
        alert("success","Le Manga à bien été ajouté");
    }
    echo "<form method='POST' action='liste_mangas.php'>";
}
initfooter();

?>